<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Entity\Innovation;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $em = $this->container->get('neo4j.manager');
        // replace this example code with whatever you need
        $i = $em->getRepository(Innovation::class)->findOneByName('roue');

        if(null === $i){
            $i = new Innovation('roue');
            $em->persist($i);
            $em->flush();
        }

        var_dump($i);
        
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);
    }
}
