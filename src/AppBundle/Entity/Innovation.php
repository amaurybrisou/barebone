<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use HireVoice\Neo4j\Annotation as OGM;

/**
 * @OGM\Entity(labels="Innovation")
 */
class Innovation
{
    /**
     * @OGM\Auto
     * @var int
     */
    private $id;

    /**
     * @OGM\Property
     * @OGM\Index
     * @var string
     */
    private $name;

    /**
     * @param string $name
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    public function getId(){
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function  getName(){
        return $this->name;
    }

    public function setName($name){
        $this->name = $name;

        return $this;
    }
}